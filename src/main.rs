use std::fs::File;
use std::io::{Read, Seek, SeekFrom};
use std::path::PathBuf;
use structopt::StructOpt;
use serde::{Deserialize, Serialize, Serializer};
use serde::ser::SerializeStruct;
use bincode::deserialize;
use std::mem::size_of;
use std::fmt;
#[derive(StructOpt)]
struct Arguments {
    device: PathBuf,
}

#[repr(C)]
#[derive(Deserialize, Debug, Default)]
struct LegacyPartition {
    status: u8,
    start_address: [u8; 3],
    kind: u8,
    end_address: [u8; 3],
    first_absolute: u32,
    number_of_sectors: u32
}

impl serde::ser::Serialize for LegacyPartition {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: serde::ser::Serializer,
        {
            let mut s = serializer.serialize_struct("LegacyPartition", 3)?;
            s.serialize_field("status", &format!("0x{:02X}", self.status))?;
            s.serialize_field("start_address", &format!("0x{:02X}{:02X}{:02X}", self.start_address[0], self.start_address[1], self.start_address[2]))?;
            s.serialize_field("end_address", &format!("0x{:02X}{:02X}{:02X}", self.end_address[0], self.end_address[1], self.end_address[2]))?;

            s.serialize_field("kind", &format!("0x{:02X}", self.kind))?;
            s.end()
        }
}

#[repr(C, packed)]
#[derive(Deserialize, Debug)]
struct Mbr {
    legacy_partitions: [LegacyPartition; 4],
    boot_signature: u16
}

const LEGACY_FIELDS : [&'static str; 4] = [
    "legacy_partition_1",
    "legacy_partition_2",
    "legacy_partition_3",
    "legacy_partition_4"];

impl serde::ser::Serialize for Mbr {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: serde::ser::Serializer,
        {
            let mut s = serializer.serialize_struct("Mbr", 0)?;
            for (i, part) in self.legacy_partitions.iter().enumerate() {
                s.serialize_field(LEGACY_FIELDS[i], part)?;// &format!("{}", part))?;
            }
            s.serialize_field("boot_signature", &format!("0x{:04X}", self.boot_signature))?;
            s.end()
        }
}

impl fmt::Display for Mbr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "{}", serde_yaml::to_string(&self).unwrap())
    }
}
 
impl fmt::Display for LegacyPartition {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "{}", serde_yaml::to_string(&self).unwrap())
    }
}

 #[derive(Deserialize)]
struct Partition {
    guid: Uuid,
    unique: Uuid,
    first_lba: u64,
    last_lba: u64,
    attributes: Attributes,
    name: Utf16Name,
}

impl serde::ser::Serialize for Partition {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: serde::ser::Serializer,
        {
            let mut s = serializer.serialize_struct("Partition", 1)?;
            s.serialize_field("guid", &self.guid)?;
            s.serialize_field("unique", &self.unique)?;
            s.serialize_field("first_lba", &format!("{0:08X} ({0:})", self.first_lba))?;
            s.serialize_field("last_lba", &format!("{0:08X} ({0:})", self.last_lba))?;
            s.serialize_field("attributes", &self.attributes)?;
            s.serialize_field("name", &self.name)?;
            s.end()
        }
}

#[derive(Deserialize)]
struct Uuid {
    uuid: [u8; 16]
}


#[derive(Deserialize)]
struct Utf16Name {
    name: [u32; 18]
}

impl serde::ser::Serialize for Utf16Name {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: serde::ser::Serializer,
        {
            let mut s = serializer.serialize_struct("Partition", 1)?;
            s.serialize_field("name", &format!(""))?;
            s.end()
        }
}

#[derive(Deserialize)]
struct Attributes {
    attributes: u64
}

impl serde::ser::Serialize for Attributes {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: serde::ser::Serializer,
        {
                let mut s = serializer.serialize_struct("Attributes", 1)?;
                s.serialize_field("attributes", &format!("0x{:016X}", self.attributes))?;
            s.end()
        }
}

impl fmt::Display for Attributes {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut fm = String::new();
        fm+= &format!("Platform: {}\n", ((self.attributes & 0x01) == 1) as bool);
        fm+= &format!("EFI: {}\n", ((self.attributes & 0x02) == 2) as bool);
        fm+= &format!("Legacy support: {}\n", ((self.attributes & 0x04) == 4) as bool);
        fm+= &format!("Partition type: {:02X}\n", self.attributes & 0xFFFF_0000_0000_0000);
        fm+= &format!("Partition: {:16X}\n", self.attributes);
        write!(f, "{}", fm)
    }
}

impl serde::ser::Serialize for Uuid {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: serde::ser::Serializer,
        {
            let mut buf = String::with_capacity(40);
            for (i, &byte) in self.uuid.iter().enumerate() {
                if i > 0 && (i%4) == 0 {
                    buf.push('-');
                }
                buf.push_str(&format!("{:02X}", byte));
            }
            let mut s = serializer.serialize_struct("Uuid", 1)?;
            s.serialize_field("uuid", &buf)?;
            s.end()
        }
}

fn main() {
    let arguments = Arguments::from_args();
    let mut file = File::open(arguments.device).expect("device open failed");

    println!("sizeof {}", size_of::<Mbr>());
    let size = size_of::<Mbr>();
    file.seek(SeekFrom::Current(512 - 66)).expect("Seek failed");
    let mut buf : [u8; size_of::<Mbr>()] = [0; size_of::<Mbr>()];
    file.read(&mut buf).expect("Could not read device");
    for (i, b) in buf.iter().enumerate() {
        if i > 0 && (i % 64) == 0 {
            println!("");
        }
        print!("{:02X}", b);
    }
    let mbr: Mbr = deserialize(&buf).expect("Deserialize failed");
    println!("{}", mbr);
    /*
    file.seek(SeekFrom::Current(512)).expect("Seek failed");
    for _ in 2..33 {
        let mut buf : [u8; size_of::<Partition>()] = [0; size_of::<Partition>()];
        file.read(&mut buf).expect("Could not read device");
        if buf[0] == 0 {
            break;
        }
        for (i, byte) in buf.iter().enumerate() {
            if i > 0 && (i%32) == 0 {
                println!("");
            }
            print!("{:02X}", byte);
        }
        println!("");
        let part : Partition = deserialize(&buf).expect("Deserialize failed");
        println!("{}", serde_yaml::to_string(&part).unwrap());
    }
    */
}
